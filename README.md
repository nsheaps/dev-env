# dev-env
A repo to contain the desired set up for [@nsheaps](https://bitbucket.org/nsheaps/)'s development environment.

Inspired by https://github.com/geerlingguy/mac-dev-playbook/. This design takes into account 1-button setup (copy/paste) and the ability to update the enviroment. This enables organizations to fork this repo and customize as necessary and push changes to developers machines to help ensure a consistent environment for their engineers.

## Used by
Do you use this? Let me know! [Shoot me an email](mailto:nsheaps@gmail.com) and I'll add your company here with links to your customized version.

## How To Run
Run the following command in terminal. OS X ONLY.

### First time
```bash
curl -s https://bitbucket.org/nsheaps/dev-env/raw/master/init.sh > /tmp/dev-env-init.sh && chmod +x /tmp/dev-env-init.sh && /tmp/dev-env-init.sh |& tee /tmp/dev-env-init.log; mv /tmp/dev-env-init.log ~/.dev-env/log/init.log;
```
This will download the `init.sh` script located in this repo folder to `/tmp/dev-env-init.sh`, then execute it, logging output, and moving it to `~/.dev-env/log/init.sh`.

The `init.sh` script does the following:

* Command Line Tools are installed
* Brew is installed and up to date
* Git is installed via brew and up to date
* Ansible is installed via brew and up to date
* Checkout this repo to `~/.dev-env/`
* Execute `~/.dev-env/bin/dev-env.sh upgrade`

### Upgrade 

#### `dev-env upgrade`
This will re-run the ansible playbook included in this repo, which is designed to the best of it's ability to preserve any modifications made since the last upgrade.

This command will `git pull origin/master` for the repo located in `~/.dev-env/`, then run the ansible playbook located at `~/.dev-env/ansible/main.yml`

This takes over your `.bash_profile`, `.bashrc` and `.zshrc` files if they exist. Make any future modifications in `.profile` if you'd like them to be preserved between dev-env upgrades.

Todo make profile template include alias to make this command work

Todo make this happen as part of `~/.bash_profile` init.


### Testing
Todo move this to travis-ci/github and mimic https://github.com/geerlingguy/mac-dev-playbook/blob/master/.travis.yml

#### Bootstrap
```bash
rm -rf ~/.dev-env/; ./init.sh |& tee /tmp/dev-env-init.log; mv /tmp/dev-env-init.log ~/.dev-env/log/init.log;
```