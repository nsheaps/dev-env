#!/usr/bin/env bash
#title           :init.sh
#description     :bootstraps the install process for getting the dev
#                 env up and running
#author		     :nsheaps@gmail.com
#usage		     :curl -s <url that holds this> | sh
#==============================================================================
set -uf -o pipefail

REMOTE="git@bitbucket.org:nsheaps/dev-env.git"
CHECKOUT_LOCATION="$HOME/.dev-env"
# duplicated here because this needs to be self contained
RED=$(tput setaf 1) # use to display error
GREEN=$(tput setaf 2) # use to display success
YELLOW=$(tput setaf 3) # use to display warn
BLUE=$(tput setaf 4)
PURPLE=$(tput setaf 5) # use to display input/output
CYAN=$(tput setaf 6) # use to display actions
WHITE=$(tput setaf 7)
GREY=$(tput setaf 8)
NORMAL=$(tput sgr0)

LOGO="$GREEN
===========
= dev-env =
===========

"



function ensureBrewPackageInstalled {
    package=$1

    printf "${WHITE}Looking for $package (via brew)...\n${GREY}"
    brew list | grep $package 2>&1>/dev/null
    if [[ $? != 0 ]]; then
        printf "${YELLOW}Could not find $package (via brew), installing...\n${GREY}"
        brew install $package
        printf "${GREEN}Installed $package (via brew)\n${GREY}"
    else
        printf "${GREEN}Found $package (via brew)\n${GREY}"
        brew info $package | head -10
        printf "${WHITE}Ensuring $package is up to date...\n${GREY}"
        OUTPUT=$(brew upgrade $package 2>&1)
        if [[ $? == 1 ]]; then
            printf "${GREY}Already up-to-date\n${WHITE}"
        else
            printf "$OUTPUT\n${WHITE}"
        fi
    fi

    printf "${WHITE}$package binary info${GREY}\n"
    $package --version
    PACKAGE_INFO=$(command -v $package | xargs ls -lha)
    printf "$PACKAGE_INFO\n"

    echo $PACKAGE_INFO | grep Cellar 2>&1>/dev/null
    if [[ $? == 1 ]]; then
        printf "${YELLOW}$package not linked to brew version, linking...\n${GREY}"
        OUTPUT=$(brew link $package 2>&1)
        printf "$OUTPUT\n"

        echo $OUTPUT | grep "Already linked" 2>&1>/dev/null
        if [[ $? != 1 ]]; then
            printf "${RED}Brew thinks $package is already linked, relinking them...${GREY}"
            brew unlink $package && brew link $package

            PACKAGE_INFO=$(command -v $package | xargs ls -lha)
            printf "$PACKAGE_INFO\n"

            echo $PACKAGE_INFO | grep Cellar 2>&1>/dev/null
            
            if [[ $? == 1 ]]; then
                printf "${RED}\nStill couldn't link the appropriate binary for $package. Please fix and try again\n${NORMAL}"
                exit 1
            fi
        fi
    else
        printf "${GREEN}$package is up to date and linked to brew\n${GREY}"
    fi
}

clear
printf "$LOGO"
printf "${WHITE}Welcome to the dev-env bootstrapper.

This program will do the following if not already done:
- install command line tools
- install brew
- install brew package git
- install brew package ansible
- checkout the source of this repo into $CHECKOUT_LOCATION
  (from: $REMOTE, requires git)
- execute \`$CHECKOUT_LOCATION/bin/dev-env.sh upgrade\` (requires ansible)

"

read -n 1 -s -r -p "${CYAN}Press any key to continue, or Ctrl-C to cancel${NORMAL}"
clear
printf "$LOGO"

printf "${WHITE}Looking for command line tools...\n${GREY}"
OUTPUT=$(xcode-select -p 2>&1)
if [[ $? == 0 ]]; then
    printf "${GREEN}Found command line tools\n${GREY}"
    printf "$OUTPUT"
    pkgutil --pkg-info=com.apple.pkg.CLTools_Executables
else
    printf "${YELLOW}Could not find command line tools, installing...\n${GREY}"
    touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress;
    PROD=$(softwareupdate -l |
        grep "\*.*Command Line" |
        head -n 1 | awk -F"*" '{print $2}' |
        sed -e 's/^ *//' |
        tr -d '\n')
    softwareupdate -i "$PROD" --verbose;
    OUTPUT=$(xcode-select -p 2>&1)

    if [[ $? == 0 ]]; then
        printf "${GREEN}Installed command line tools\n${GREY}"
    else
        printf "${RED}Failed to install command line tools. Try installing them manually and try again.\n${NORMAL}"
        exit 1
    fi
fi

printf "${WHITE}Looking for brew...\n${GREY}"
if command -v brew 2>&1>/dev/null; then
    printf "${GREEN}Found brew\n${GREY}"
    brew --version
    printf "${WHITE}Ensuring brew is up to date...\n${GREY}"
    brew update
    printf "${WHITE}"
else
    printf "${YELLOW}Could not find brew, installing...\n${GREY}"
    # </dev/null makes this an unattended install
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" </dev/null
    printf "${GREEN}Installed brew\n${GREY}"
fi

ensureBrewPackageInstalled git
ensureBrewPackageInstalled ansible

printf "${WHITE}Checking out $REMOTE to $CHECKOUT_LOCATION\n${GREY}"
if [[ ! -d $CHECKOUT_LOCATION ]]; then
    git clone $REMOTE ${CHECKOUT_LOCATION}
    if [[ $? == 0 ]]; then
        printf "${GREEN}Checked out $REMOTE to $CHECKOUT_LOCATION\n${GREY}"
    else
        printf "${RED}Failed to checkout $REMOTE to $CHECKOUT_LOCATION\n${NORMAL}"
        exit 1
    fi
else
    printf "${RED}Found pre-existing folder at $CHECKOUT_LOCATION. Either:\n a) Remove the folder and try again, or\n b) update via the update command for your current version (eg \`dev-env update\`)\n${NORMAL}"
    exit 1
fi

cd ${CHECKOUT_LOCATION}
printf "\n${WHITE}Bootstrap complete. Executing \`$CHECKOUT_LOCATION/bin/dev-env update\`...\n${NORMAL}"


./bin/dev-env.sh upgrade

exit $?

# update brew

# install the basics to continue
# git
# ansible

# checkout the repo into ~/dev/dev-env
# cd to dir
# run update.sh
# - check to see if repo updated
# - run ansible-playbook
# - - create user data data for population of configs
# - - - Full Name
# - - - email
# - - - github username/pw (memory only to set up github?)
# - - set up github?
# - - install oh my zsh
# - - create ssh keys
# - - automate ssh upload?
# - - - github
# - - - bitbucket
# - - copy .bashrc, .bash_profile, .zshrc, oh-my-zsh config
# - - copy git configs
# - - ensure brew packages present
# - - - google-chrome
# - - - visual-studio-code
# - - - docker
# - - - wget
# - - - aws-cli? (pip version instead?)
# - - install node@8.x
# - - ensure node packages present
# - - - n
# - - - eslint
# - - - NO PM2, THIS IS A PROJECT DEP
# - - install python@2.x?


